import { Inject, Injectable } from '@angular/core';
import { Http,RequestOptions } from '@angular/http'
import { HttpClient,HttpRequest,HttpHeaders,HTTP_INTERCEPTORS} from '@angular/common/http'
import { Observable } from 'rxjs/observable'
import 'rxjs/Rx'
import { IGetResponse } from './models/interfaces'


@Injectable()
export class UserService {
  constructor(@Inject('API') private Api,private http:HttpClient) { 
    
  }

  getUsers():Observable<any>{
    
    let url=`${this.Api}user/pas`;

    //let url=`${this.Api}?email=muratkirmizigul@gmail.com`;


    return this.http.get(url);
  }
}
