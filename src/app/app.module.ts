import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';


/*Components*/
import { AnaSayfaComponent } from './ana-sayfa/ana-sayfa.component';
import { UstMenuComponent } from './ust-menu/ust-menu.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserItemComponent } from './user-item/user-item.component'

/*Services*/
import { MessageService } from "./message.service"
import { UserService } from "./user.service"

import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';


/* Environments */

import {environment} from '../environments/environment'

import {AuthInterceptor} from './auth-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    AnaSayfaComponent,
    UstMenuComponent,
    UserListComponent,
    UserDetailsComponent,
    UserFormComponent,
    UserItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    NgbModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    {provide:'API',useValue:environment.API},
    { provide: HTTP_INTERCEPTORS, 
      useClass: AuthInterceptor, 
      multi: true 
    },
    MessageService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
