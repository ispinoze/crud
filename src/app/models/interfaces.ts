
export interface IUser {
    id: number,
    updatedAt: string | Date,
    createdAt: string | Date,
    email: string,
    username: string,
    roles: IRole[],//any,
    new?: boolean,
    edited?: boolean
    
}

export interface IRole{
    id: number,
    description: string,
    enabled: true,
    name: string,
    updatedAt: string | Date,
    createdAt: string | Date
}

export interface IGetResponse{
    totalElements: number,
    totalPages: number,
    last: boolean,
    size: number,
    number: number,
    sort?: null | string,
    numberOfElements: number,
    first : boolean
}