import { Component, OnInit } from '@angular/core';
import {IUser} from '../models/interfaces'
import {UserService} from '../user.service'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users:IUser[]


  constructor(private _userService:UserService) {

   }

  ngOnInit() {
    this.getUsers();
  }

  private getUsers(){
    return this._userService.getUsers()
    .subscribe(_res => {
      this.users=_res.content
    });
  }
}
