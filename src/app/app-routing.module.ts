import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AnaSayfaComponent} from './ana-sayfa/ana-sayfa.component'

const routes: Routes = [
  {
    path: '',
    component:AnaSayfaComponent,
    children: []
  },
  {
    path: '**',redirectTo:'/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
