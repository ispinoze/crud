import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor} 
     from "@angular/common/http";
import {HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    
    constructor() {
    }

    intercept(req: HttpRequest<any>, 
               next: HttpHandler):Observable<HttpEvent<any>> {
        
        const clonedRequest = req.clone({
           setHeaders:{
                Authorization:'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1ZGllbmNlIjoid2ViIiwiY3JlYXRlZCI6MTUwNTA3NDEzNjQ0MSwiaXNzIjoiQWx0xLFuZGHEnyBCZWxlZGl5ZXNpLUJpbGdpIMSwxZ9sZW0gTcO8ZMO8cmzDvMSfw7wiLCJleHAiOjE1MDUwNzU5MzZ9.Ffz089_gBOPoiEGrR01Aoi3BCLrQFjBWtWm8qQCDdLwWl745gfg3GnZTyo_BLqqvx5BwWJb4mXiqMnSUiGVg9w'
            }      
        });
        return next.handle(clonedRequest);
    }
}